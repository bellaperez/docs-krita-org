msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: en Waveletdecompose Krita image menuselection\n"
"X-POFile-SpellExtra: wavelets Wavelets guilabel images filters\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/filters/wavelet_decompose.rst:None
msgid ".. image:: images/filters/Wavelet_decompose.png"
msgstr ".. image:: images/filters/Wavelet_decompose.png"

#: ../../reference_manual/filters/wavelet_decompose.rst:1
msgid "Overview of the wavelet decompose in Krita."
msgstr "Introdução à decomposição por ondas ('wavelets') no Krita."

#: ../../reference_manual/filters/wavelet_decompose.rst:11
#: ../../reference_manual/filters/wavelet_decompose.rst:16
msgid "Wavelet Decompose"
msgstr "Decomposição de Ondas"

#: ../../reference_manual/filters/wavelet_decompose.rst:18
msgid ""
"Wavelet decompose uses wavelet scales to turn the current layer into a set "
"of layers with each holding a different type of pattern that is visible "
"within the image. This is used in texture and pattern making to remove "
"unwanted noise quickly from a texture."
msgstr ""
"A decomposição por ondas usa escalas de ondas ('Wavelets') sobre um conjunto "
"de camadas, onde cada uma possui um tipo diferente de padrão que está "
"visível dentro da imagem. Isto é usado na criação de texturas e padrões de "
"forma a remover rapidamente o ruído indesejado de uma textura."

#: ../../reference_manual/filters/wavelet_decompose.rst:20
msgid "You can find it under :menuselection:`Layers`."
msgstr "Podê-la-á encontrar nas :menuselection:`Camadas`."

#: ../../reference_manual/filters/wavelet_decompose.rst:22
msgid ""
"When you select it, it will ask for the amount of wavelet scales. More "
"scales, more different layers. Press :guilabel:`OK`, and it will generate a "
"group layer containing the layers with their proper blending modes:"
msgstr ""
"Quando a seleccionar, irá pedir a quantidade de escalas de ondas. Quanto "
"mais escalas, mais camadas diferentes. Carregue em :guilabel:`OK`, para que "
"ele vá gerir uma camada de grupo que contém as camadas com os seus próprios "
"modos de mistura:"

#: ../../reference_manual/filters/wavelet_decompose.rst:27
msgid ""
"Adjust a given layer with middle gray to neutralize it, and merge everything "
"with the :guilabel:`Grain Merge` blending mode to merge it into the end "
"image properly."
msgstr ""
"Ajustar uma dada camada com um cinzento intermédio para a neutralizar e "
"reunir tudo com o modo de mistura :guilabel:`Junção do Grão` para a reunir "
"adequadamente com a imagem final."
