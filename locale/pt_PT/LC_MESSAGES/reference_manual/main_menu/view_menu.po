# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:27+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: softproofing displaysettings icons Krita image\n"
"X-POFile-SpellExtra: snapping kbd Kritamouseright Gamute images alt\n"
"X-POFile-SpellExtra: instantpreview ref mouseright guilabel\n"

#: ../../<generated>:1
msgid "Show Painting Previews"
msgstr "Mostrar as Antevisões da Pintura"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/main_menu/view_menu.rst:1
msgid "The view menu in Krita."
msgstr "O menu Ver no Krita."

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "View"
msgstr "Ver"

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "Wrap around mode"
msgstr "Modo de envolvência"

#: ../../reference_manual/main_menu/view_menu.rst:16
msgid "View Menu"
msgstr "O Menu Ver"

#: ../../reference_manual/main_menu/view_menu.rst:18
msgid "Show Canvas Only"
msgstr "Mostrar Apenas a Área de Desenho"

#: ../../reference_manual/main_menu/view_menu.rst:19
msgid ""
"Only shows the canvas and what you have configured to show in :guilabel:"
"`Canvas Only` settings."
msgstr ""
"Só mostra a área de desenho e o que tiver configurado para mostrar na "
"configuração :guilabel:`Apenas da Área de Desenho`."

#: ../../reference_manual/main_menu/view_menu.rst:20
msgid "Fullscreen mode"
msgstr "Modo de ecrã completo"

#: ../../reference_manual/main_menu/view_menu.rst:21
msgid "This will hide the system bar."
msgstr "Isto irá esconder a barra do sistema."

#: ../../reference_manual/main_menu/view_menu.rst:22
msgid "Wrap Around Mode"
msgstr "Modo de Envolvência"

#: ../../reference_manual/main_menu/view_menu.rst:23
msgid ""
"This will show the image as if tiled orthographically. Very useful for "
"tiling 3d textures. Hit the :kbd:`W` key to quickly activate it."
msgstr ""
"Isto irá mostrar a imagem como se estivesse sobre um padrão ortográfico. É "
"muito útil para criar padrões de texturas 3D. Carregue em :kbd:`W` para o "
"activar rapidamente."

#: ../../reference_manual/main_menu/view_menu.rst:24
msgid "Instant Preview"
msgstr "Antevisão Instantânea"

#: ../../reference_manual/main_menu/view_menu.rst:25
msgid "Toggle :ref:`instant_preview` globally."
msgstr "Activar/desactivar a :ref:`instant_preview` a nível global."

#: ../../reference_manual/main_menu/view_menu.rst:26
msgid "Soft Proofing"
msgstr "Prova Suave"

#: ../../reference_manual/main_menu/view_menu.rst:27
msgid "Activate :ref:`soft_proofing`."
msgstr "Activa a :ref:`soft_proofing`."

#: ../../reference_manual/main_menu/view_menu.rst:28
msgid "Out of Gamut Warnings"
msgstr "Avisos Fora do Gamute"

#: ../../reference_manual/main_menu/view_menu.rst:29
msgid "See the :ref:`soft_proofing` page for details."
msgstr "Veja mais detalhes na página :ref:`soft_proofing`."

#: ../../reference_manual/main_menu/view_menu.rst:30
msgid "Canvas"
msgstr "Área de Desenho"

#: ../../reference_manual/main_menu/view_menu.rst:31
msgid "Contains view manipulation actions."
msgstr "Contém as acções de manipulação da janela."

#: ../../reference_manual/main_menu/view_menu.rst:32
msgid "Mirror View"
msgstr "Espelho"

#: ../../reference_manual/main_menu/view_menu.rst:33
msgid ""
"This will mirror the view. Hit the :kbd:`M` key to quickly activate it. Very "
"useful during painting."
msgstr ""
"Isto irá criar um espelho da janela. Carregue em :kbd:`M` para activá-lo "
"rapidamente. Muito útil quando estiver a pintar."

#: ../../reference_manual/main_menu/view_menu.rst:34
msgid "Show Rulers"
msgstr "Mostrar as Réguas"

#: ../../reference_manual/main_menu/view_menu.rst:35
msgid ""
"This will display a set of rulers. |mouseright| the rulers after showing "
"them, to change the units."
msgstr ""
"Isto irá mostrar um conjunto de réguas. Carregue com o |mouseright| sobre as "
"regras, após estarem visíveis, para mudar as unidades."

#: ../../reference_manual/main_menu/view_menu.rst:36
msgid "Rulers track pointer"
msgstr "As réguas seguem o cursor"

#: ../../reference_manual/main_menu/view_menu.rst:37
msgid ""
"This adds a little marker to the ruler to show where the mouse is in "
"relation to them."
msgstr ""
"Isto adiciona um pequeno marcador à régua para mostrar onde está o rato em "
"relação a ela."

#: ../../reference_manual/main_menu/view_menu.rst:38
msgid "Show Guides"
msgstr "Mostrar as Guias"

#: ../../reference_manual/main_menu/view_menu.rst:39
msgid "Show or hide the guides."
msgstr "Mostra ou esconde as guias."

#: ../../reference_manual/main_menu/view_menu.rst:40
msgid "Lock Guides"
msgstr "Bloquear as Guias"

#: ../../reference_manual/main_menu/view_menu.rst:41
msgid "Prevent the guides from being able to be moved by the cursor."
msgstr "Impede as guias de serem movidas pelo cursor."

#: ../../reference_manual/main_menu/view_menu.rst:42
msgid "Show Status Bar"
msgstr "Mostrar a Barra de Estado"

#: ../../reference_manual/main_menu/view_menu.rst:43
msgid ""
"This will show the status bar. The status bar contains a lot of important "
"information, a zoom widget, and the button to switch Selection Display Mode."
msgstr ""
"Isto irá mostrar a barra de estado. Esta contém diversas informações "
"importantes, um item de ampliação e o botão para mudar o Modo de "
"Visualização da Selecção."

#: ../../reference_manual/main_menu/view_menu.rst:44
msgid "Show Grid"
msgstr "Mostrar a Grelha"

#: ../../reference_manual/main_menu/view_menu.rst:45
msgid "Shows and hides the grid. Shortcut: :kbd:`Ctrl + Shift + '`"
msgstr "Mostra e esconde a grelha. :kbd:`Ctrl + Shift + '`"

#: ../../reference_manual/main_menu/view_menu.rst:46
msgid "Show Pixel Grid"
msgstr "Mostrar a Grelha de Pontos"

#: ../../reference_manual/main_menu/view_menu.rst:47
msgid "Show the pixel grid as configured in the :ref:`display_settings`."
msgstr ""
"Mostra a grelha de pontos configurada de acordo com as :ref:"
"`display_settings`."

#: ../../reference_manual/main_menu/view_menu.rst:48
msgid "Snapping"
msgstr "Ajuste"

#: ../../reference_manual/main_menu/view_menu.rst:49
msgid "Toggle the :ref:`snapping` types."
msgstr "Activa/desactiva os tipos de :ref:`snapping`."

#: ../../reference_manual/main_menu/view_menu.rst:50
msgid "Show Painting Assistants"
msgstr "Mostrar os Assistentes de Pintura"

#: ../../reference_manual/main_menu/view_menu.rst:51
msgid "Shows or hides the Assistants."
msgstr "Mostra ou esconde os Assistentes."

#: ../../reference_manual/main_menu/view_menu.rst:53
msgid "Shows or hides the Previews."
msgstr "Mostra ou esconde as Antevisões."
