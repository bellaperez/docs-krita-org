msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___curve_engine."
"pot\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:1
msgid "The Curve Brush Engine manual page."
msgstr "介绍 Krita 的曲线笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:18
msgid "Curve Brush Engine"
msgstr "曲线笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:21
msgid ".. image:: images/icons/curvebrush.svg"
msgstr ".. image:: images/icons/curvebrush.svg"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:22
msgid ""
"The curve brush is a brush engine which creates strokes made of evenly "
"spaced lines. It has, among other things been used as a replacement for "
"pressure sensitive strokes in lieu of a tablet."
msgstr ""
"曲线笔刷引擎是通过无数微小等距曲线构成一条笔画的笔刷引擎。在没有数位板的条件"
"下，它可以被用来模拟具有压力感应的曲线效果。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:25
msgid "Settings"
msgstr "设置"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:27
msgid ""
"First off, the line produced by the Curve brush is made up of 2 sections:"
msgstr "此笔刷引擎绘制的线条由两部分组成，要注意这两部分的名字比较容易混淆："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:29
msgid "The connection line, which is the main line drawn by your mouse"
msgstr "连接线，实际上是笔画两边的主干线，由鼠标绘制。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:31
msgid ""
"The curve lines I think, which are the extra fancy lines that form at "
"curves. The curve lines are formed by connecting one point of the curve to a "
"point earlier on the curve. This also means that if you are drawing a "
"straight line, these lines won't be visible, since they'll overlap with the "
"connection line. Drawing faster gives you wider curves areas."
msgstr ""
"曲线，是连接着两条主干线之间的网状曲线，每条曲线连接着两条主干线上的前后两"
"点。如果你画出一条直线，这些曲线就看不到了，而整个笔画就会失去宽度。绘制转弯"
"笔画时的速度越快，画出来的曲线区域就越宽。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:35
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:36
msgid ""
"You have access to 3 settings from the Lines tab, as well as 2 corresponding "
"dynamics:"
msgstr "在曲线笔刷引擎的“参数”页面有三组选项，引擎可以使用 2 个动态："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:38
msgid ""
"Line width: this applies to both the connection line and the curve lines."
msgstr "线条宽度：影响连接线和曲线的宽度。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:40
msgid "Line width dynamics: use this to vary line width dynamically."
msgstr ""
"线条宽度动态：选项列表中的“线条宽度”页面可以用传感器控制线条宽度的动态变化。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:42
msgid ""
"History size: this determines the distance for the formation of curve lines."
msgstr "历史大小：控制曲线的形成距离。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:44
msgid ""
"If you set this at low values, then the curve lines can only form over a "
"small distances, so they won't be too visible."
msgstr "如果此数值较低，则曲线只能在两条连接线相近的点处形成，曲线不会很明显。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:45
msgid ""
"On the other hand, if you set this value too high, the curve lines will only "
"start forming relatively \"late\"."
msgstr "而如果此数值较高，曲线的生成会有延迟。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:46
msgid ""
"So in fact, you'll get maximum curve lines area with a mid-value of say... "
"40~60, which is about the default value. Unless you're drawing at really "
"high resolutions."
msgstr ""
"一般来说，要想得到最宽的笔画，可以将此数值设为 40 至 60，接近中间值。如果图像"
"分辨率高，可以酌情调整。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:48
msgid ""
"Curves opacity: you can't set different line widths for the connection line "
"and the curve lines, but you can set a different opacity for the curve "
"lines. With low opacity, this will produce the illusion of thinner curve "
"lines."
msgstr ""
"曲线不透明度：虽然你无法单独指定连接线和曲线的线条宽度，但你可以单独指定曲线"
"的不透明度，从而营造曲线较细较淡的效果。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:50
msgid "Curves opacity dynamics: use this to vary Curves opacity dynamically."
msgstr "曲线不透明度动态：通过传感器来控制不透明度的动态变化。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:52
msgid "In addition, you have access to two checkboxes:"
msgstr "除此之外你还有两个选项可用："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:54
msgid ""
"Paint connection line, which toggles the visibility of the connection line"
msgstr "绘制连接线：控制连接线 (两条主干线) 的绘制与否。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:55
msgid ""
"Smoothing, which... I have no idea actually. I don't see any differences "
"with or without it. Maybe it's for tablets?"
msgstr ""
"平滑：可以让曲线的效果更平滑，但效果并不明显。在下面的例子中从上到下依次展示"
"了线条宽度、历史大小、曲线不透明度、关闭绘制连接线的效果。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:58
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:60
msgid "Drawing variable-width lines"
msgstr "如何绘制具有宽度变化的笔画"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:62
msgid ""
"And here's the only section of this tutorial that anyone cares about: pretty "
"lineart lines! For this:"
msgstr "下面我们将介绍如何使用此笔刷引擎画出好看的线稿曲线："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:64
msgid ""
"Use the Draw Dynamically mode: I tend to increase drag to at least 50. Vary "
"Mass and Drag until you get the feel that's most comfortable for you."
msgstr ""
"使用工具箱中的“力学笔刷工具”，至少把它工具选项中的“拽引”增加到 0.50，你可以对"
"重量和拽引数值进行微调以适应你的习惯。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:67
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:68
msgid ""
"Set line width to a higher value (ex.: 5), then turn line width dynamics on:"
msgstr "把线条宽度设为较高数值，如 5，然后勾选“线条宽度”的动态选项："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:70
msgid ""
"If you're a tablet user, just set this to Pressure (this should be selected "
"by default so just turn on the Line Width dynamics). I can't check myself, "
"but a tablet user confirmed to me that it works well enough with Draw "
"Dynamically."
msgstr ""
"如果你使用数位板，可以把线条宽度映射到压力传感器，这也是勾选“线条宽度”时的默"
"认值。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:71
msgid ""
"If you're a mouse user hoping to get variable line width, set the Line Width "
"dynamics to Speed."
msgstr "如果你使用鼠标，可以把线条宽度映射到速度传感器。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:74
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:75
msgid ""
"Set Curves opacity to 0: This is the simplest way to turn off the Curve "
"lines. That said, leaving them on will get you more \"expressive\" lines."
msgstr ""
"将曲线不透明度设为 0 可以提高线条轮廓的一致性。如果保持默认值，则可以画出更为"
"有表现力的线条。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:78
msgid "Additional tips:"
msgstr "使用技巧："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:80
msgid "Zig-zag a lot if you want a lot of extra curves lines."
msgstr "如果想要生成更多曲线，就多点扭动笔画。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:81
msgid ""
"Use smooth, sweeping motions when you're using Draw Dynamically with Line "
"Width set to Speed: abrupt speed transitions will cause abrupt size "
"transitions. It takes a bit of practice, and the thicker the line, the more "
"visible the deformities will be. Also, zoom in to increase control."
msgstr ""
"如果使用速度传感器，请尽可能使用均一的速度和平滑的手势绘制线条，突然的速度变"
"化会导致线条宽度的大幅波动，线条越粗，这种波动就越明显。你也可以放大视图来方"
"便掌控。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:82
msgid ""
"If you need to vary between thin and thick lines, I suggest creating presets "
"of different widths, since you can't vary the base line width from the "
"canvas."
msgstr ""
"因为你无法在画布上直接更改线条宽度，如果你需要使用几种不同粗细的线条，那么最"
"好把它们制作成不同的笔刷预设。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:84
msgid "Alternative:"
msgstr "替代配置方案："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:86
msgid "Use the Draw Dynamically mode"
msgstr "使用力学笔刷工具。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:87
msgid "Set Curves opacity to 100"
msgstr "将曲线不透明度设为 100。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:88
msgid "Optionally decrease History size to about 30"
msgstr "将历史大小降到 30 左右 (可选)。"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:90
msgid ""
"The curve lines will fill out the area they cover completely, resulting in a "
"line with variable widths. Anyway, here are some comparisons:"
msgstr ""
"这样曲线就会把它们覆盖的区域完全填充，画出一条带有宽度变化的实线。在下面的比"
"较中，左一为线条宽度映射到速度、手绘笔刷、曲线不透明度 0；左二为线条宽度映射"
"到速度、力学笔刷、曲线不透明度 0；左三为力学笔刷、默认曲线不透明度；下面为力"
"学笔刷、曲线不透明度 100："

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:93
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:94
msgid "And here are examples of what you can do with this brush:"
msgstr "下面是此笔刷引擎的实际使用效果："
