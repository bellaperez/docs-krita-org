msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_contributors_manual___community.pot\n"

#: ../../contributors_manual/community.rst:1
msgid "Guide to the Krita community"
msgstr "Krita 社区工作指引"

#: ../../contributors_manual/community.rst:10
msgid "community"
msgstr "社区"

#: ../../contributors_manual/community.rst:10
msgid "communication"
msgstr "联系"

#: ../../contributors_manual/community.rst:16
msgid "The Krita Community"
msgstr "Krita 社区工作"

#: ../../contributors_manual/community.rst:18
msgid ""
"Get in touch! Apart from the website at https://www.krita.org, the Krita "
"project has three main communication channels:"
msgstr ""
"Krita 社区的参与者应该保持联系。除了https://www.krita.org 网站外，Krita项目还"
"有三个主要的联系渠道："

#: ../../contributors_manual/community.rst:20
msgid "Internet Relay Chat (IRC)"
msgstr "互联网中继聊天 (IRC)"

#: ../../contributors_manual/community.rst:21
msgid "The mailing list"
msgstr "邮件列表"

#: ../../contributors_manual/community.rst:22
#: ../../contributors_manual/community.rst:86
msgid "Phabricator"
msgstr "Phabricator 网站"

#: ../../contributors_manual/community.rst:24
msgid ""
"While Krita developers and users are present on social media such as "
"Twitter, Mastodon, Reddit, Google+, Tumblr or Facebook, those are not the "
"place where we discuss new features, bugs, development or where we make "
"plans for the future."
msgstr ""
"虽然 Krita 的开发者和用户会使用 Twitter、Mastodon、Reddit, Google+、Tumblr "
"或 Facebook 等社交媒体，但它们并不是我们讨论新功能、程序问题、软件开发或制定"
"未来计划的地方。"

#: ../../contributors_manual/community.rst:26
msgid "There are also the:"
msgstr "除了上面三个主要的联系渠道外，我们还有下面的交流方式："

#: ../../contributors_manual/community.rst:28
msgid "bug tracker"
msgstr "程序问题追踪器"

#: ../../contributors_manual/community.rst:29
msgid "development sprints"
msgstr "开发冲刺活动"

#: ../../contributors_manual/community.rst:31
msgid ""
"You’ll find that there are a number of people are almost always around: the "
"core team."
msgstr ""
"在这些地方，你会发现总有那么几个人特别活跃，他们便是 Krita 的核心团队。"

#: ../../contributors_manual/community.rst:33
msgid ""
"Boudewijn (irc: boud): project maintainer, lead developer. Works full-time "
"on Krita. Manages the Krita Foundation, triages bugs, does social media and "
"admin stuff. Boudewijn is also on Reddit as boudewijnrempt."
msgstr ""
"Boudewijn (irc: boud)：项目维护人员、主程序员。为 Krita 全职工作。管理Krita基"
"金会，分拣程序问题，负责社交媒体和管理员事务。Boudewijn 的 Reddit 用户名是 "
"boudewijnrempt。"

#: ../../contributors_manual/community.rst:34
msgid "Dmitry (irc: dmitryk_log): lead developer. Works full-time on Krita."
msgstr "Dmitry (irc：dmitryk_log)：主程序员。为 Krita 全职工作。"

#: ../../contributors_manual/community.rst:35
msgid ""
"Wolthera (irc: wolthera_laptop): developer, writes the manual and tutorials, "
"triages bugs, helps people out"
msgstr ""
"Wolthera (irc：wolthera_laptop)：程序员。编写手册和教程，分拣程序问题，帮助大"
"家解决问题。"

#: ../../contributors_manual/community.rst:36
msgid "Scott Petrovic (irc: scottyp): UX designer, developer, webmaster"
msgstr "Scott Petrovic (irc：scottyp)：UX 设计人员，程序员，网站管理员。"

#: ../../contributors_manual/community.rst:37
msgid ""
"David Revoy (irc: deevad): expert user, creates Pepper & Carrot, maintains "
"the preset bundle."
msgstr ""
"David Revoy (irc: deevad)：资深用户，创作了自由漫画 Pepper & Carrot，维护 "
"Krita 的资源包。"

#: ../../contributors_manual/community.rst:38
msgid "Alvin Wong (irc: windragon): windows guru"
msgstr "Alvin Wong (irc: windragon)：Windows 专家。"

#: ../../contributors_manual/community.rst:39
msgid "Ben Cooksley (irc: bcooksley): KDE system administrator."
msgstr "Ben Cooksley (irc: bcooksley)：KDE 系统管理员。"

#: ../../contributors_manual/community.rst:41
msgid ""
"Krita’s team spans the globe, but most development happens in Europe and "
"Russia."
msgstr "Krita 的团队人员遍布全球，但大部分的开发工作在欧洲和俄罗斯进行。"

#: ../../contributors_manual/community.rst:43
msgid ""
"Krita is part of the larger KDE community. The KDE® Community is a free "
"software community dedicated to creating an open and user-friendly computing "
"experience, offering an advanced graphical desktop, a wide variety of "
"applications for communication, work, education and entertainment and a "
"platform to easily build new applications upon. The KDE contributors guide "
"is relevant for Krita contributors, too, and can be found `here <https://"
"archive.flossmanuals.net/kde-guide/>`_."
msgstr ""
"Krita 项目是 KDE 社区大集体的一员。KDE® 社区是一个自由软件社区，致力于创建开"
"放且用户友好的计算体验，它提供了先进的图形化桌面环境，一系列用于通信、办公、"
"教育和娱乐领域的应用程序以及一套能够轻松构建应用程序的软件平台。KDE 贡献者指"
"引也适用于 Krita 项目，可通过 `此链接 <https://archive.flossmanuals.net/kde-"
"guide/>`_ 查看。"

#: ../../contributors_manual/community.rst:45
msgid ""
"The Krita Foundation was created to support development of Krita. The Krita "
"Foundation has sponsored Dmitry’s work on Krita since 2013."
msgstr ""
"Krita 基金会是为了支持 Krita 项目的发展而成立的。自2013年以来 Krita 基金会一"
"直在赞助 Dmitry 为 Krita 项目进行的工作。"

#: ../../contributors_manual/community.rst:48
msgid "Internet Relay Chat"
msgstr "互联网中继聊天"

#: ../../contributors_manual/community.rst:50
msgid ""
"IRC is the main communication channel. There are IRC clients for every "
"operating system out there, as well as a web client on the krita website."
msgstr ""
"IRC 是 Krita 项目主要的通信渠道。每种操作系统都有适用的 IRC 客户端，Krita 网"
"站上也有一个 IRC 网页客户端。"

#: ../../contributors_manual/community.rst:52
msgid ""
"Joining IRC: connect to irc.freenode.net, select a unique nickname and join "
"the #krita and ##krita-chat channels. #krita is for on-topic talk, ##krita-"
"chat for off-topic chat."
msgstr ""
"加入IRC：连接到 irc.freenode.net，选择一个与众不同的昵称，然后加入 #krita 和 "
"##krita-chat 频道。#krita 是项目事务的讨论频道，##krita-chat 则是闲聊频道。"

#: ../../contributors_manual/community.rst:53
msgid "Don’t ask to ask: if you’ve got a question, just ask it."
msgstr ""

#: ../../contributors_manual/community.rst:54
msgid ""
"Don’t panic if several discussions happen at the same time. That’s normal in "
"a busy channel."
msgstr ""
"如果看到有好几个不同的讨论在同时进行，别慌。在业务繁忙的频道里这是家常便饭。"
"看清楚你的谈话对象，@ 他们的用户名就行了。"

#: ../../contributors_manual/community.rst:55
msgid "Talk to an individual by typing their nick and a colon."
msgstr "输入某人的昵称外加一个冒号即可与其进行交谈。"

#: ../../contributors_manual/community.rst:56
msgid ""
"Almost every Monday, at 14:00 CET or CEST, we have a meeting where we "
"discuss what happened in the past week, what we’re doing, and everything "
"that’s relevant for the project. The meeting notes are kept in google docs."
msgstr ""
"我们会在几乎每个周一的  CET 或 CEST 时间14:00 举行一场会议，讨论上一周发生的"
"事情，我们正在做的事情，还有本项目的一切相关事务。会议记录以 Google Docs 的形"
"式保存。"

#: ../../contributors_manual/community.rst:57
msgid ""
"Activity is highest in CET or CEST daytime and evenings. US daytime and "
"evenings are most quiet."
msgstr ""
"Krita 的各个频道在 CET 或 CEST 时间的早上和晚间最为活跃，而在美国时间的早上和"
"晚间最为安静。"

#: ../../contributors_manual/community.rst:58
msgid ""
"**IRC is not logged. If you close the channel, you will be gone, and you "
"will not be able to read what happened when you join the channel again. If "
"you ask a question, you have to stay around!**"
msgstr ""
"**IRC 不会保存记录。频道窗口一旦关闭，你将离开对话，在下次登录之前你将无法读"
"取频道内发生的一切。如果你在 IRC 中提出了问题，你必须保持登录状态才能得到回"
"答。**"

#: ../../contributors_manual/community.rst:59
msgid ""
"It is really irritating for other users and disrupting to conversations if "
"you keep connecting and disconnecting."
msgstr ""
"如果你不停地连接和断开，对于其他用户来说是一种滋扰，也会打断别人的对话。如果"
"网络连接实在不稳定，请使用其他联系渠道。"

#: ../../contributors_manual/community.rst:63
msgid "Mailing List"
msgstr "邮件列表"

#: ../../contributors_manual/community.rst:65
msgid ""
"The mailing list is used for announcements and sparingly for discussions. "
"Everyone who wants to work on Krita one way or another should be subscribed "
"to the mailing list."
msgstr ""
"邮件列表用于发表公告，也会被零星地用于讨论。所有打算为 Krita 项目进行贡献的参"
"与者都应该订阅我们的邮件列表。"

#: ../../contributors_manual/community.rst:67
msgid ""
"`Mailing List Archives <https://mail.kde.org/mailman/listinfo/kimageshop>`_"
msgstr "`邮件列表存档 <https://mail.kde.org/mailman/listinfo/kimageshop>`_"

#: ../../contributors_manual/community.rst:69
msgid ""
"The mailing list is called \"kimageshop\", because that is the name under "
"which the Krita project was started. Legal issues (surprise!) led to two "
"renames, once to Krayon, then to Krita."
msgstr ""
"该邮件列表叫做 \"kimageshop\"，它是 Krita 项目最早的名称。因为商标问题，项目"
"的名称发生过两次更改，先是改为 Krayon，后来才改为 Krita。"

#: ../../contributors_manual/community.rst:73
msgid "Gitlab (KDE Invent)"
msgstr ""

#: ../../contributors_manual/community.rst:75
msgid "Gitlab serves the following purposes for the Krita team:"
msgstr ""

#: ../../contributors_manual/community.rst:77
msgid ""
"Review volunteers' submissions: https://invent.kde.org/kde/krita/"
"merge_requests for the code itself, https://invent.kde.org/websites/docs-"
"krita-org/merge_requests for the content of the Krita Manual."
msgstr ""

#: ../../contributors_manual/community.rst:78
msgid ""
"Host the code git repository: https://invent.kde.org/kde/krita . Note that "
"while there are mirrors of our git repository on Github and Phabricator, we "
"do not use them for Krita development."
msgstr ""

#: ../../contributors_manual/community.rst:79
msgid ""
"Host the Krita Manual content repository: https://invent.kde.org/websites/"
"docs-krita-org"
msgstr ""

#: ../../contributors_manual/community.rst:81
msgid "**Do not** make new issues on Gitlab or use it to make bug reports."
msgstr ""

#: ../../contributors_manual/community.rst:83
msgid ""
"**Do** put all your code submissions (merge requests) on Gitlab. **Do not** "
"attach patches to bugs in the bug tracker."
msgstr ""

#: ../../contributors_manual/community.rst:88
msgid "Phabricator serves the following purposes for the Krita team:"
msgstr "KDE 的 Phabricator 网站为 Krita 团队提供了以下功能："

#: ../../contributors_manual/community.rst:90
msgid ""
"Track what we are working on: https://phabricator.kde.org/maniphest/ This "
"includes development tasks, designing new features and UX design, as well as "
"tasks related to the website."
msgstr ""
"跟踪我们正在进行的工作：https://phabricator.kde.org/maniphest/ 包括开发任务、"
"设计新功能、UX 设计和与网站有关的任务。"

#: ../../contributors_manual/community.rst:92
msgid ""
"**Do not** report bugs as tasks on Phabricator. Phabricator is where we "
"organize our work."
msgstr ""
"**不要** 把程序问题当成任务在 Phabricator 上面发布。Phabricator 是我们组织协"
"调工作的场所。"

#: ../../contributors_manual/community.rst:95
msgid "Bugzilla: the Bug Tracker"
msgstr "Bugzilla：程序问题跟踪器"

#: ../../contributors_manual/community.rst:97
msgid ""
"Krita shares the bug tracker with the rest of the KDE community. Krita bugs "
"are found under the Krita product. There are two kinds of reports in the bug "
"tracker: bugs and wishes. See the chapters on :ref:`Bug Reporting "
"<bugs_reporting>` and :ref:`Bug Triaging <triaging_bugs>` on how to handle "
"bugs. Wishes are feature requests. Do not report feature requests in "
"bugzilla unless a developer has asked you to. See the chapter on :ref:"
"`Feature Requests <developing_features>` for what is needed to create a good "
"feature request."
msgstr ""
"Krita 与 KDE 社区的其他项目共用一个程序问题跟踪器。Krita 的程序问题被放在在 "
"Krita 产品项目里。程序问题跟踪器中的报告可以分为两类：问题 (bugs) 和愿望 "
"(wishes)。程序问题的报告流程请参阅 KDE 文档的“程序问题报告”和“程序问题分拣”章"
"节。愿望报告其实就是新功能请求。除非有开发人员让你这样做，否则不要在 "
"Bugzilla 里面报告新功能请求。关于如何创建一个好的新功能请求，参见 KDE 文档"
"的“新功能请求”章节。"

#: ../../contributors_manual/community.rst:100
msgid "Sprints"
msgstr "开发冲刺活动"

#: ../../contributors_manual/community.rst:102
msgid ""
"Sometimes, core Krita developers and users come together, most often in "
"Deventer, the Netherlands, to work together on our code design, UX design, "
"the website or whatever needs real, face-to-face contact. Travel to sprints "
"is usually funded by KDE e.V., while accommodation is funded by the Krita "
"Foundation."
msgstr ""
"有些时候 Krita 的核心开发人员会聚集起来，面对面地集中处理程序代码、UX 设计、"
"网站等事务。这样的活动叫做“开发冲刺活动”，通常在荷兰的代芬特尔进行。参加活动"
"的旅费通常由 KDE e.V. 资助，而住宿费用则由 Krita 基金会资助。"
