# Translation of docs_krita_org_user_manual___mirror_tools.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:26+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/mirror_tools.rst:1
msgid "How to use the canvas mirroring tools in Krita."
msgstr "Com s'utilitzen les eines d'emmirallar el llenç en el Krita."

#: ../../user_manual/mirror_tools.rst:11
msgid "Mirror"
msgstr "Emmirallar"

#: ../../user_manual/mirror_tools.rst:11
msgid "Symmetry"
msgstr "Simetria"

#: ../../user_manual/mirror_tools.rst:16
msgid "Mirror Tools"
msgstr "Eines d'emmirallar"

#: ../../user_manual/mirror_tools.rst:18
msgid ""
"Draw on one side of a mirror line while the Mirror Tool copies the results "
"to the other side. The Mirror Tools are accessed along the toolbar. You can "
"move the location of the mirror line by grabbing the handle."
msgstr ""
"Dibuixeu en un costat d'una línia d'emmirallar mentre que l'eina "
"d'emmirallar copiarà el resultat a l'altre costat. A les eines d'emmirallar "
"s'hi accedeix a través de la barra d'eines. Podreu moure la ubicació de la "
"línia d'emmirallar prenent la nansa."

#: ../../user_manual/mirror_tools.rst:21
msgid ".. image:: images/Mirror-tool.png"
msgstr ".. image:: images/Mirror-tool.png"

#: ../../user_manual/mirror_tools.rst:22
msgid ""
"Mirror Tools give a similar result to the :ref:`multibrush_tool`, but unlike "
"the Multibrush which only traces brush strokes like the :ref:"
"`freehand_brush_tool`, the Mirror Tools can be used with any other tool that "
"traces strokes, such as the :ref:`line_tool` and the :ref:`path_tool`, and "
"even with the Multibrush Tool."
msgstr ""
"Les eines d'emmirallar donen un resultat similar a l':ref:`multibrush_tool`, "
"però a diferència del pinzell múltiple, el qual només fa pinzellades com l':"
"ref:`freehand_brush_tool`, les eines d'emmirallar es poden utilitzar amb "
"qualsevol altra eina que faci traços de pinzell, com ara l':ref:`line_tool` "
"i l':ref:`path_tool`, i fins i tot amb l'eina de pinzell múltiple."

#: ../../user_manual/mirror_tools.rst:24
msgid ""
"**Horizontal Mirror Tool** - Mirror the results along the horizontal axis."
msgstr ""
"**Eina d'emmirallament horitzontal**: Reflecteix el resultat al llarg de "
"l'eix horitzontal."

#: ../../user_manual/mirror_tools.rst:27
msgid "**Vertical Mirror Tool** - Mirror the results along the vertical axis."
msgstr ""
"**Eina d'emmirallament vertical**: Reflecteix el resultat al llarg de l'eix "
"vertical."

#: ../../user_manual/mirror_tools.rst:30
msgid ""
"There are additional options for each tool. You can access these by the "
"clicking the drop-down arrow located on the right of each tool."
msgstr ""
"Hi ha opcions addicionals per a cada eina. Hi podreu accedir fent clic a la "
"fletxa desplegable situada a la dreta de cada eina."

#: ../../user_manual/mirror_tools.rst:33
msgid ""
"Hide Mirror Line (toggle) -- Locks the mirror axis and hides the axis line."
msgstr ""
"Oculta la línia X/Y del mirall (alternar): bloqueja l'eix del mirall i "
"oculta la línia de l'eix."

#: ../../user_manual/mirror_tools.rst:35
msgid "Lock (toggle) - hides the move icon on the axis line."
msgstr ""
"Desbloqueja/Bloqueja (alternar): oculta la icona de moviment a la línia de "
"l'eix."

#: ../../user_manual/mirror_tools.rst:36
msgid ""
"Move to Canvas Center - Moves the axis line to the center of the canvas."
msgstr "Mou al centre del llenç: mou la línia de l'eix al centre del llenç."

#: ../../user_manual/mirror_tools.rst:40
msgid "Mirroring along a rotated line"
msgstr "Emmirallar al llarg d'una línia de gir"

#: ../../user_manual/mirror_tools.rst:42
msgid ""
"The Mirror Tool can only mirror along a perfectly vertical or horizontal "
"line. To mirror along a line that is at a rotated angle, use the :ref:"
"`multibrush_tool` and its various parameters, it has more advanced options "
"besides basic symmetry."
msgstr ""
"L'eina d'emmirallar només pot reflectir al llarg d'una línia perfectament "
"vertical o horitzontal. Per a reflectir al llarg d'una línia que es troba en "
"un angle girat, utilitzeu l':ref:`multibrush_tool` i els seus diversos "
"paràmetres, disposa d'opcions més avançades a més de la simetria bàsica."
