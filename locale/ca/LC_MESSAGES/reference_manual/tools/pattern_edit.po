# Translation of docs_krita_org_reference_manual___tools___pattern_edit.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-25 13:29+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: eina vectorial d'edició de patrons"

#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr "Referència de l'eina vectorial Edició de patrons del Krita."

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Pattern"
msgstr "Patró"

#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr "Eina d'edició de patrons"

#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr "|toolpatternedit|"

#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""
"L'eina per a l'edició de patrons es va eliminar en la versió 4.0, actualment "
"no hi ha manera d'editar emplena amb un patró per als vectors."

#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""
"L'eina per a l'edició de patrons funciona sobre les Formes vectorials que "
"utilitzen Emplena amb un patró. En aquestes formes, l'eina d'edició de "
"patrons permet canviar la mida, la proporció i l'origen d'un patró."

#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr "Editar sobre el llenç"

#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""
"Podeu canviar l'origen fent clic i arrossegant el node superior, això només "
"és possible en el mode En mosaic."

#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""
"Podeu canviar la mida i la proporció fent clic i arrossegant el node "
"inferior. No hi ha manera de restringir la proporció en l'edició sobre el "
"llenç, això només és possible en els modes Original i En mosaic."

#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr "Hi ha diverses Opcions de l'eina per a un ajust fi:"

#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr "Primer hi ha les opcions de Patró."

#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr "Es pot establir a:"

#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr "Original:"

#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr "Només mostrarà una còpia del patró sense estirar."

#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr "En mosaic (predeterminat):"

#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr "Permetrà que el patró aparegui en mosaic a les direccions «X» i «Y»."

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr "Repetició:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr "Estira:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr "Estirarà la imatge del Patró fins a la forma."

#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr "Origen del patró. Es pot establir a:"

#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr "Superior esquerra"

#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr "Superior"

#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr "Superior dreta"

#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr "Esquerra"

#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr "Centre"

#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr "Dreta"

#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr "Inferior esquerra"

#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr "Inferior"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr "Punt de referència:"

# skip-rule: punctuation-period
#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr "Inferior dreta"

#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr "Per ajustos addicionals, establir en percentatges."

#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr "X:"

#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr "Desplaçament en la coordenada «X», de manera horitzontal."

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr "Desplaçament del punt de referència:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr "Y:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr "Desplaçament en la coordenada «Y», de manera vertical."

#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr "Desplaçament del mosaic:"

#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr "El desplaçament del mosaic si el patró està en mosaic."

#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr "Ajustament fi del canvi de mida del patró."

#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr "Amp.:"

#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr "L'amplada en píxels."

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr "Mida del patró:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr "Alç.:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr "L'alçada en píxels."

#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
"I després hi ha els :guilabel:`Patrons`, el qual és un acoblador de mini "
"patrons, i on podreu seleccionar el patró utilitzat per a emplenar."
